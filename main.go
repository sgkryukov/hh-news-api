package main

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic(err)
	}
	defer db.Close()

	router := gin.Default()

	router.GET("/api/v1/news", func(c *gin.Context) {
		if c.Request.Header.Get("X-Token") != "tPK7s7vdmDxZf7Ar" {
			c.JSON(401, gin.H{
				"error": gin.H{
					"message": "Ошибка авторизации",
					"code":    401,
				},
			})
		} else {

			news := []News{}
			page, err := strconv.ParseInt(c.DefaultQuery("page", "1"), 0, 64)
			if err != nil || page < 1 {
				page = 1
			}

			limit, err := strconv.ParseInt(c.DefaultQuery("limit", "10"), 0, 64)
			if err != nil || limit < 1 {
				limit = 10
			}
			if limit > 20 {
				limit = 20
			}

			db.Limit(limit).Offset((page - 1) * limit).Find(&news)

			if len(news) > 0 {
				c.JSON(200, gin.H{
					"data": news,
				})
			} else {
				c.JSON(404, gin.H{
					"error": gin.H{
						"message": "Новости не найдены",
						"code":    404,
					},
				})
			}

		}
	})

	router.GET("/api/v1/news/:id", func(c *gin.Context) {
		if c.Request.Header.Get("X-Token") != "tPK7s7vdmDxZf7Ar" {
			c.JSON(401, gin.H{
				"error": gin.H{
					"message": "Ошибка авторизации",
					"code":    401,
				},
			})
		} else {

			id := c.Param("id")

			news := NewsDetails{}

			db.First(&news, id)

			if news.ID > 0 {
				spotlight := []News{}

				if news.ID < 22 {
					db.Limit(3).Where("id > ?", news.ID).Find(&spotlight)
				} else if news.ID == 22 {
					db.Limit(3).Where("id IN (1,23,24)").Find(&spotlight)
				} else if news.ID == 23 {
					db.Limit(3).Where("id IN (1,2,24)").Find(&spotlight)
				} else if news.ID == 24 {
					db.Limit(3).Where("id IN (1,2,3)").Find(&spotlight)
				}

				c.JSON(200, gin.H{
					"data":      news,
					"spotlight": spotlight,
				})
			} else {
				c.JSON(404, gin.H{
					"error": gin.H{
						"message": "Новость не найдена",
						"code":    404,
					},
				})
			}
		}
	})

	router.Run(":8880")
}
