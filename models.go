package main

import "time"

type News struct {
	ID          uint      `json:"id"`
	CreatedAt   time.Time `json:"created_at"`
	Title       string    `json:"title"`
	Image       string    `json:"image"`
	Thumbnail   string    `json:"thumbnail"`
	Description string    `json:"description"`
	Source      string    `json:"-"`
	Text        string    `json:"-"`
	Lead        string    `json:"-"`
}

type NewsDetails struct {
	ID     uint   `json:"id"`
	Source string `json:"source"`
	Text   string `json:"text"`
	Lead   string `json:"lead"`
}

func (NewsDetails) TableName() string {
	return "news"
}
