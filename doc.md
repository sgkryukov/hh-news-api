FORMAT: 1A
HOST: http://news.mhth.ru

# API Documentation v1


# GET /api/v1/news?page=2&limit=3

+ Parameters
    + page: `2` (integer, optional) – номер страницы
        + Default: `1`
    + limit: `3` (integer, optional) – количесвто элементов на страницу, не более 20
        + Default: `10`

+ Request

    + Headers

            X-Token: secret_key



+ Response 200 (application/json)


    + Body

            {"data":[{"id":4,"created_at":"2017-01-26T13:37:44.145527348+03:00","title":"ATQUE AUT","image":"http://placehold.it/350x200/92BAE0.png?text=350x100","thumbnail":"http://placehold.it/100/92BAE0.png?text=100x100","description":"velit harum facilis quisquam vel aut! quia reiciendis qui veritatis ea occaecati delectus tenetur et. non maiores suscipit velit eaque ipsam. officia molestiae ullam saepe laudantium vel! dolorum tempore occaecati voluptatum et sit exercitationem. minima modi sequi dolorem nihil. ut numquam voluptas voluptas."},{"id":5,"created_at":"2017-01-26T13:37:44.146897665+03:00","title":"ET MOLLITIA REM OMNIS REM","image":"http://placehold.it/350x200/92BAE0.png?text=350x100","thumbnail":"http://placehold.it/100/92BAE0.png?text=100x100","description":"nam iusto eligendi vel. eaque sit consectetur maxime. fugiat culpa atque voluptatem eum est ut aspernatur totam et."},{"id":6,"created_at":"2017-01-26T13:37:44.148532435+03:00","title":"VOLUPTAS DOLOR QUIA EVENIET","image":"http://placehold.it/350x200/92BAE0.png?text=350x100","thumbnail":"http://placehold.it/100/92BAE0.png?text=100x100","description":"amet similique ipsa est quibusdam. quo omnis sunt vero omnis fugit asperiores vero. corrupti provident necessitatibus voluptatem numquam aut occaecati! incidunt ipsum maxime aut ad molestias et nisi. blanditiis qui ipsa fuga corporis officiis expedita. dolorem eveniet id eos nihil omnis facere. alias et accusantium natus perferendis quibusdam. quis et architecto et eos est aut dolores."}]}


+ Response 401 (application/json)


    + Body

            {"error":{"code":401,"message":"Ошибка авторизации"}}


+ Response 404 (application/json)


    + Body

            {"error":{"code":404,"message":"Новости не найдены"}}





# GET /api/v1/news/:id

+ Parameters
    + id: `24` (integer) - Id новости

+ Request

    + Headers

            X-Token: secret_key



+ Response 200 (application/json)


    + Body

            {"data":{"id":24,"source":"http://www.medsolutions.ru/news/3024","text":"\u003cb\u003eDOLORES REPREHENDERIT HIC SED ADIPISCI\u003c/b\u003e\u003cp\u003e\u003cem\u003enisi aut velit...\u003c/em\u003e vero at dicta. magnam cum incidunt. maiores et occaecati similique et optio. blanditiis sed impedit. veniam necessitatibus provident et et asperiores et quasi. id ut repellat dolorum. nisi est dolores possimus provident.\u003c/p\u003e\u003cp\u003elibero ut expedita totam quam. eius ex quia molestiae vero sed. aut dicta cumque enim non quia! vitae assumenda nihil accusamus consequatur facilis. fuga soluta mollitia sit aut aliquid recusandae. eos asperiores ipsa ad aspernatur!\u003c/p\u003e\u003cp\u003eaut repellat eligendi hic id autem. maiores et unde voluptatum dolores accusantium nobis consequatur ipsam dolor. quasi hic non enim quia aliquid ullam beatae fuga. voluptas architecto voluptatem sapiente autem velit omnis. sit est ipsam voluptas corrupti.\u003c/p\u003e\u003col\u003e\u003cli\u003eest excepturi nulla est\u003c/li\u003e\u003cli\u003eest quasi a rerum id\u003c/li\u003e\u003c/ol\u003e\u003cp\u003eenim repellat voluptatem. qui dicta sed deleniti. sunt voluptates ea deserunt nihil. eligendi non quia reprehenderit veniam. et facilis aliquid perferendis officia qui eos assumenda. fuga assumenda assumenda maxime id dolor velit. consectetur eos non sed eius hic accusamus deleniti repudiandae.\u003c/p\u003e\u003cp\u003ebeatae iste optio. debitis blanditiis qui eveniet. nesciunt doloremque ullam debitis nihil voluptate nobis!\u003c/p\u003e\u003cp\u003equo sequi sint soluta reiciendis facilis.\u003c/p\u003e\u003cul\u003e\u003cli\u003evoluptas unde doloremque expedita\u003c/li\u003e\u003cli\u003esint\u003c/li\u003e\u003c/ul\u003e\u003cp\u003ereiciendis et provident cumque odio laboriosam! debitis dignissimos commodi nulla consequuntur est. quis non non reprehenderit dolores vitae quis iure alias. maxime ipsa earum et sunt error placeat dolor. praesentium magnam quaerat repellat possimus ab esse. aut illo rem voluptas et laborum. qui maxime sint ratione corrupti omnis ullam.\u003c/p\u003e\u003cp\u003equidem officia ipsam voluptas. ex et architecto at earum aut fuga. qui aut corrupti veritatis qui. vitae quis sapiente assumenda! ratione vel deserunt voluptatem facilis dolore. ut repudiandae velit laudantium id libero. voluptas blanditiis aut excepturi. enim nemo ipsam quibusdam ducimus.\u003c/p\u003e","lead":"\u003cp\u003e\u003cstrgong\u003eest non ratione rerum\u003c/strong\u003e non libero consequatur sapiente dignissimos. sequi eius qui impedit enim. laborum quo aut deleniti commodi aut tempora praesentium. quis nostrum sequi. rerum inventore vel et.\u003c/p\u003e"},"spotlight":[{"id":1,"created_at":"2017-01-26T13:37:44.140266411+03:00","title":"DOLORES BEATAE CUM AUTEM","image":"http://placehold.it/350x200/92BAE0.png?text=350x100","thumbnail":"http://placehold.it/100/92BAE0.png?text=100x100","description":"ea aut velit qui sed nesciunt. aut ad mollitia molestias aut et! similique ea eveniet ea odio. excepturi et pariatur laudantium sed."},{"id":2,"created_at":"2017-01-26T13:37:44.142087291+03:00","title":"RATIONE ILLO VEL REM","image":"http://placehold.it/350x200/92BAE0.png?text=350x100","thumbnail":"http://placehold.it/100/92BAE0.png?text=100x100","description":"soluta iste debitis asperiores eius. dignissimos nulla quo ut dolores. et harum vel ut adipisci cupiditate. est tenetur labore qui hic qui. voluptatem est et. delectus dignissimos fuga dolorem alias provident! blanditiis omnis nemo quod omnis ab sint porro voluptates. eum repudiandae et debitis eos deleniti."},{"id":3,"created_at":"2017-01-26T13:37:44.143869673+03:00","title":"SAEPE VOLUPTATUM DUCIMUS VOLUPTATEM","image":"http://placehold.it/350x200/92BAE0.png?text=350x100","thumbnail":"http://placehold.it/100/92BAE0.png?text=100x100","description":"dolor et cupiditate ut nemo repellat. enim laborum sunt consequatur itaque. sit autem voluptas quas dolor et voluptas qui. ut ex quaerat et alias unde corporis. quam sit consequatur recusandae voluptate velit repellendus mollitia nesciunt. ducimus culpa ab quas sequi."}]}



+ Response 401 (application/json)


    + Body

            {"error":{"code":401,"message":"Ошибка авторизации"}}


+ Response 404 (application/json)


    + Body

            {"error":{"code":404,"message":"Новость не найдена"}}
