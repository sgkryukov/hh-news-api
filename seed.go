package main

import (
	"fmt"

	"github.com/icrowley/fake"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func Seed() {
	db, err := gorm.Open("sqlite3", "test.db")
	if err != nil {
		panic("failed to connect database")
	}
	defer db.Close()

	// Migrate the schema
	db.AutoMigrate(&News{})

	err = fake.SetLang("ru")
	if err != nil {
		panic(err)
	}

	for i := 1; i < 25; i++ {
		text := fmt.Sprintf("<b>%v</b>", fake.Title())
		text = fmt.Sprintf("%v", text)
		text = fmt.Sprintf("%v<p><em>%v...</em> %v</p>", text, fake.Words(), fake.Paragraph())
		text = fmt.Sprintf("%v<p>%v</p>", text, fake.Paragraph())
		text = fmt.Sprintf("%v<p>%v</p>", text, fake.Paragraph())
		text = fmt.Sprintf("%v<ol><li>%v</li><li>%v</li></ol>", text, fake.Words(), fake.Words())
		text = fmt.Sprintf("%v<p>%v</p>", text, fake.Paragraph())
		text = fmt.Sprintf("%v<p>%v</p>", text, fake.Paragraph())
		text = fmt.Sprintf("%v<p>%v</p>", text, fake.Paragraph())
		text = fmt.Sprintf("%v<ul><li>%v</li><li>%v</li></ul>", text, fake.Words(), fake.Words())
		text = fmt.Sprintf("%v<p>%v</p>", text, fake.Paragraph())
		text = fmt.Sprintf("%v<p>%v</p>", text, fake.Paragraph())

		db.Create(&News{
			Title:       fake.Title(),
			Image:       "http://placehold.it/350x200/92BAE0.png?text=350x100",
			Thumbnail:   "http://placehold.it/100/92BAE0.png?text=100x100",
			Description: fake.Paragraph(),
			Source:      fmt.Sprintf("http://www.medsolutions.ru/news/30%02d", i),
			Text:        text,
			Lead:        fmt.Sprintf("<p><strgong>%v</strong> %v</p>", fake.Words(), fake.Paragraph()),
		})
	}
	// Create

	//   // Read
	//   var product Product
	//   db.First(&product, 1) // find product with id 1
	//   db.First(&product, "code = ?", "L1212") // find product with code l1212

	//   // Update - update product's price to 2000
	//   db.Model(&product).Update("Price", 2000)

	//   // Delete - delete product
	//   db.Delete(&product)
}
